package com.iwec.model;

public class Airplane {
	private int id;
	private String manufacturer;
	private String model;
	private int seats;
	private int capacityOfFuel;
	
	
	public Airplane() {
		super();
	}


	public Airplane(int id, String manufacturer, String model, int seats, int capacityOfFuel) {
		super();
		this.id = id;
		this.manufacturer = manufacturer;
		this.model = model;
		this.seats = seats;
		this.capacityOfFuel = capacityOfFuel;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getManufacturer() {
		return manufacturer;
	}


	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}


	public String getModel() {
		return model;
	}


	public void setModel(String model) {
		this.model = model;
	}


	public int getSeats() {
		return seats;
	}


	public void setSeats(int seats) {
		this.seats = seats;
	}


	public int getCapacityOfFuel() {
		return capacityOfFuel;
	}


	public void setCapacityOfFuel(int capacityOfFuel) {
		this.capacityOfFuel = capacityOfFuel;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Airplane other = (Airplane) obj;
		if (id != other.id)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Airplane [id=" + id + ", manufacturer=" + manufacturer + ", model=" + model + ", seats=" + seats
				+ ", capacityOfFuel=" + capacityOfFuel + "]";
	}
	
	
	

}
