package com.iwec.model;

public class AirplaneData {
 private int id;
 private int seats;
 private int capacityofFuel;
 
 public AirplaneData() {
	super();
}

public AirplaneData(int id, int seats, int capacityOfSeats) {
	super();
	this.id = id;
	this.seats = seats;
	this.capacityofFuel = capacityOfSeats;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public int getSeats() {
	return seats;
}

public void setSeats(int seats) {
	this.seats = seats;
}

public int getCapacityOfFuel() {
	return capacityofFuel;
}

public void setCapacityOfFuel(int capacityOfFuel) {
	this.capacityofFuel = capacityOfFuel;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + id;
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	AirplaneData other = (AirplaneData) obj;
	if (id != other.id)
		return false;
	return true;
}

@Override
public String toString() {
	return "AirplaneData [id=" + id + ", seats=" + seats + ", capacityOfFuel=" + capacityofFuel + "]";
}
 
 
}
