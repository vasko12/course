package com.iwec.main;

import java.util.List;

import com.iwec.Reader.AirplaneReader;
import com.iwec.Reader.AirplaneReaderFW;
import com.iwec.Reader.ReaderCsv;
import com.iwec.calculations.Filter;
import com.iwec.model.Airplane;
import com.iwec.model.AirplaneData;

public class App {

	public static void main(String[] args) {
		ReaderCsv<AirplaneData> reader = new AirplaneReaderFW();
		List<AirplaneData> list = reader.read("airplaneData");
		list.stream().forEach(System.out::println);
		
		System.out.println();
		ReaderCsv<Airplane> r = new AirplaneReader();
		List<Airplane> l = r.read("airplanes.csv");
		l.stream().forEach(System.out::println);
		
		
	}
	

}
