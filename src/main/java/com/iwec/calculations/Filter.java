package com.iwec.calculations;

import java.util.LinkedList;
import java.util.List;

import com.iwec.Reader.AirplaneReader;
import com.iwec.Reader.AirplaneReaderFW;
import com.iwec.Reader.ReaderCsv;
import com.iwec.model.Airplane;
import com.iwec.model.AirplaneData;

public class Filter {
	ReaderCsv<Airplane> airp = new AirplaneReader();
	List <Airplane> alist= airp.read("airplanes.csv");
	
	ReaderCsv<AirplaneData> adata= new AirplaneReaderFW();
	List<AirplaneData> dlist = adata.read("airplaneData.txt");
	
	CalcPercentage calculate = new CalcPercentage();
	
	public static List<Airplane> result = new LinkedList<>();
	
	public void filter() {
		for (int i=0; i<=3; i++) {
			float passengerPerc=calculate.calcPercentage(Float.valueOf(dlist.get(i).getSeats()), Float.valueOf(alist.get(i).getSeats()));
			float fuelPerc=calculate.calcPercentage(Float.valueOf(dlist.get(i).getCapacityOfFuel()), Float.valueOf(alist.get(i).getCapacityOfFuel()));
			if(passengerPerc>=80.0f && fuelPerc>=70.0f) {
				result.add(alist.get(i));
			}
			else
			{
				continue;
			}
		
		}
	}
}