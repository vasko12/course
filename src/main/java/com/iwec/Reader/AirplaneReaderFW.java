package com.iwec.Reader;

import com.iwec.model.AirplaneData;
import com.iwec.wrapper.MyWrapper;

public class AirplaneReaderFW extends ReaderCsvImpl<AirplaneData> {

	@Override
	public AirplaneData createInstance(String line) {
		if (line==null || line.isEmpty()) {
			return null;
	} 
		
		AirplaneData data = new AirplaneData();
		data.setId(MyWrapper.parseInt(line.substring(0, 1)));
		data.setSeats(MyWrapper.parseInt(line.substring(1, 3)));
		data.setCapacityOfFuel(MyWrapper.parseInt(line.substring(3, 6)));
		return data;
	

}
}
