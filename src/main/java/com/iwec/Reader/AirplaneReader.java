package com.iwec.Reader;

import com.iwec.model.Airplane;
import com.iwec.wrapper.MyWrapper;

public class AirplaneReader extends ReaderCsvImpl<Airplane> {

	@Override
	public Airplane createInstance(String line) {
		if (line==null || line.isEmpty()) {
			return null;
		}
		String[] tokens = line.split(",");
		
		if(tokens.length !=5) {
			return null;
		}
		int id = MyWrapper.parseInt(tokens[0].trim());
		int seats= MyWrapper.parseInt(tokens[3].trim());
		int capacityOfFuel=MyWrapper.parseInt(tokens[4].trim());
		return new Airplane(id,tokens[1].trim(), tokens[2].trim(), seats, capacityOfFuel);
	}

}
